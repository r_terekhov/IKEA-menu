import { action, observable, when, toJS } from "mobx";

const currentCategory = observable.box("");
const setCurrentCategory = action((route) => currentCategory.set(route));

const menuNavs = observable([]);
const setMenuNavs = action((navs) => {
  while (menuNavs.length !== 0) {
    menuNavs.pop();
  }
  navs.forEach((nav) => {
    menuNavs.push(nav);
  });
});
when(
  () => menuNavs.length === 0,
  () => {
    fetch("https://ikeabackend.avocadoonline.company/category/getAll")
      .then((response) => {
        if (!response.ok) throw Error(response.statusText);
        return response.json();
      })
      .then(({ response }) => {
        const navs = response.map(({ name, urlPhoto }, index) => ({
          name,
          id: index + 1,
          photo:
            urlPhoto === "url"
              ? "https://images.pexels.com/photos/277253/pexels-photo-277253.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
              : urlPhoto,
        }));
        navs.unshift({
          name: "Специальные предложения",
          id: 0,
          photo:
            "https://firebasestorage.googleapis.com/v0/b/ikea-20c86.appspot.com/o/restaurant-menu%2ForiginalPhotos%2F%D0%94%D0%B5%D1%82%D1%81%D0%BA%D0%B8%D0%B9%20%D0%BE%D0%B1%D0%B5%D0%B4%20%D1%81%20%D0%B8%D0%B3%D1%80%D1%83%D1%88%D0%BA%D0%BE%D0%B9.jpg?alt=media&token=ea040fdb-9af8-45f8-9231-6b35523f892e",
        });
        setMenuNavs(navs);
      })
      .catch((error) => console.log(error));
  }
);

const menuPositions = observable([]);
const setMenuPositions = action((positions) => {
  while (menuPositions.length !== 0) {
    menuPositions.pop();
  }
  positions.forEach((position) => {
    menuPositions.push(position);
  });
});
when(
  () => menuPositions.length === 0,
  () => {
    fetch("https://ikeabackend.avocadoonline.company/product/getAll")
      .then((response) => {
        if (!response.ok) throw Error(response.statusText);
        return response.json();
      })
      .then(({ response }) => {
        const positions = response.map((pos) => ({
          id: pos.id,
          price: pos.cost,
          category: pos.category,
          photo: pos.urlPhoto,
          type: pos.type,
          title: pos.name,
          description: pos.description,
          allergens: pos.allergens,
          calories: pos.bgu ? pos.bgu.calories : undefined,
          consist: pos.consist,
          proteins: pos.bgu ? pos.bgu.proteins : undefined,
          fats: pos.bgu ? pos.bgu.fats : undefined,
          portionSize: pos.portionSize,
          carbohydrates: pos.bgu ? pos.bgu.carbohydrates : undefined,
          extend: pos.extendsOf ? pos.extendsOf : [],
        }));
        setMenuPositions(positions);
      })
      .catch((error) => console.log(error));
  }
);

when(
  () => menuPositions.length > 0,
  () => {
    fetch(
      "https://ikeabackend.avocadoonline.company/product/getAllSpecialOffers"
    )
      .then((response) => {
        if (!response.ok) throw Error(response.statusText);
        return response.json();
      })
      .then(({ response }) => {
        const offers = response.map((pos) => ({
          id: pos.id,
          price: pos.cost,
          category: "Специальные предложения",
          photo: pos.urlPhoto,
          title: pos.name,
          extend: pos.extendsOf ? pos.extendsOf : [],
          description: pos.description,
        }));
        setMenuPositions([...offers, ...toJS(menuPositions)]);
      })
      .catch((error) => console.log(error));
  }
);

const getPositionsByCategory = (category) => {
  const positionsJS = toJS(menuPositions);
  if (category === "") {
    return positionsJS;
  } else {
    return positionsJS.filter((pos) => pos.category === category);
  }
};
const getPositionsByQuery = (query) => {
  const copy = toJS(menuPositions);
  return copy.filter(
    (pos) =>
      pos.title.toLowerCase().includes(query.toLowerCase()) &&
      query !== "" &&
      query.length > 1 &&
      pos.category
  );
};
const getPositionById = (id) => {
  const positionsJS = toJS(menuPositions);
  const position = positionsJS.find((pos) => pos.id === id);
  return position;
};
const getPositionsByIds = (ids) => {
  return ids.map((id) => getPositionById(id));
};

export const menuStore = {
  currentCategory,
  setCurrentCategory,
  menuNavs,
  menuPositions,
  getPositionsByCategory,
  getPositionsByQuery,
  getPositionById,
  getPositionsByIds,
};
