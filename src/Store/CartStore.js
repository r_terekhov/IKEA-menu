import { action, observable, when, toJS, computed } from "mobx";
import lodash from "lodash";

const saved = observable([]);
const setSaved = action((fromCart) => {
  while (saved.length > 0) saved.pop();
  fromCart.forEach((from) => saved.push(from));
});
const getSavedGroupedByQuantity = () => {
  const copy = toJS(saved);
  const grouped = lodash.groupBy(copy, (position) => {
    const extendId = position.extend
      .map(({ position }) => position.id)
      .join("");
    return position.id + extendId;
  });
  return Object.values(grouped)
    .map((group) => ({
      quantity: group.length,
      position: group[0],
    }))
    .sort((a, b) => {
      const aId = a.position.id;
      const bId = b.position.id;
      if (aId < bId) return -1;
      if (aId > bId) return 1;
      return 0;
    });
};
const totalPrice = computed(() => {
  return toJS(saved).reduce((acc, { price }) => acc + price, 0);
});

const addToCart = action((position) => {
  const savedCopy = toJS(saved);
  savedCopy.push(position);
  setSaved(savedCopy);
  const saveObject = { lastUpdate: new Date(), saved };
  localStorage.setItem("positions", JSON.stringify(saveObject));
});
const removeFromCart = action((position) => {
  let savedCopy = toJS(saved);
  let foundIndex = savedCopy.findIndex((pos) => pos.id === position.id);
  if (foundIndex === -1) {
    return;
  } else {
    savedCopy = savedCopy.filter((_, index) => index !== foundIndex);
  }
  setSaved(savedCopy);
  localStorage.setItem("positions", JSON.stringify(saved));
});

when(
  () => saved.length === 0,
  () => {
    const fromStorage = JSON.parse(localStorage.getItem("positions"));
    if (fromStorage === null) return;
    let { lastUpdate, saved } = fromStorage;
    lastUpdate = new Date(lastUpdate);
    const currentDate = new Date();
    const hours12 = 12 * 60 * 60 * 1000;
    if (currentDate - lastUpdate > hours12) return;
    setSaved(saved);
  }
);

export const cartStore = {
  saved,
  getSavedGroupedByQuantity,
  totalPrice,
  addToCart,
  removeFromCart,
};
