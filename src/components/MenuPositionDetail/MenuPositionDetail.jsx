import React, { useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { useParams, withRouter, useHistory } from "react-router-dom";
import ShowMore from "react-show-more";
import { ShoppingCartButton } from "../ShoppingCartButton/ShoppingCartButton";
import { AdditionPopup } from "../AdditionPopup/AdditionPopup";
import "./MenuPositionDetail.css";

export const MenuPositionDetail = withRouter(
  inject("rootStore")(
    observer((props) => {
      let { id } = useParams();
      const getPositionById = props.rootStore.menuStore.getPositionById;
      const addToCart = props.rootStore.cartStore.addToCart;
      const position = getPositionById(id);
      const {
        price = 0,
        photo = "https://cdn.shopify.com/s/files/1/0533/2089/files/placeholder-images-image_large.png?format=jpg&quality=90&v=1530129081",
        title = "",
        description = "",
        allergens = "",
        calories = 0,
        consist = "",
        proteins = 0,
        fats = 0,
        carbohydrates = 0,
        category = "",
        extend = [],
      } = position || {};

      const [showAddition, setShowAddition] = useState(false);
      const history = useHistory();
      const goBack = () => {
        history.goBack();
      };
      const fromCart = props.location.search === "?fromCart=true";

      useEffect(() => {
        window.scrollTo(0, 0);
        if (fromCart) {
          extend && setShowAddition(true);
        }
      }, [extend, fromCart]);

      return (
        <div className="position-relative position-detail-wrapper">
          <div className="position-detail-button-back" onClick={goBack}>
            <i class="fas fa-chevron-left"></i>
          </div>
          <div className="position-detail-cart-button">
            <ShoppingCartButton />
          </div>
          <div className="container">
            <div className="px-5 pt-5">
              <div className="w-100 mt-1">
                <div className="row d-flex align-items-center justify-content-center">
                  <div className="col-12 col-md-5">
                    <div className="position-detail-image-container">
                      <img src={photo} className="h-100 w-100" alt={title} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="p-4">
              <div className="w-100 mt-1">
                <h2 className="text-center position-detail-title my-3">
                  {title}
                </h2>
                <p className="position-detail-description mb-1">
                  {description}
                </p>
                {price > 0 && (
                  <h2 className="position-detail-price">{price} ₽</h2>
                )}
                {category !== "Специальные предложения" &&
                  allergens &&
                  consist &&
                  calories > 0 &&
                  proteins > 0 &&
                  fats > 0 &&
                  carbohydrates > 0 && (
                    <div className="position-detail-collapse-wrapper mt-2">
                      <div>
                        <button
                          className="position-detail-collapse-button py-2"
                          type="button"
                          data-toggle="collapse"
                          data-target="#collapseExample"
                          aria-expanded="false"
                          aria-controls="collapseExample"
                        >
                          Подробнее
                        </button>
                      </div>
                      <div
                        className="collapse pb-3 position-detail-collapse-content my-2"
                        id="collapseExample"
                      >
                        <div className="row">
                          {allergens && (
                            <div className="col-12">
                              <p className="position-detail-collapse-content-text mb-1">
                                <span className="position-detail-span-bold">
                                  Аллергены
                                </span>
                              </p>
                              <p
                                className="position-detail-collapse-content-text"
                                style={{ fontSize: "20px" }}
                              >
                                <ShowMore
                                  more="Читать далее"
                                  less=""
                                  anchorClass="read-more-button"
                                >
                                  {allergens}
                                </ShowMore>
                              </p>
                            </div>
                          )}
                          {consist && (
                            <div className="col-12">
                              <p className="position-detail-collapse-content-text mb-1">
                                <span className="position-detail-span-bold">
                                  Состав
                                </span>
                              </p>
                              <p
                                className="position-detail-collapse-content-text"
                                style={{ fontSize: "20px" }}
                              >
                                <ShowMore
                                  more="Читать далее"
                                  less=""
                                  anchorClass="read-more-button"
                                >
                                  {consist}
                                </ShowMore>
                              </p>
                            </div>
                          )}
                        </div>
                        {calories > 0 &&
                          proteins > 0 &&
                          fats > 0 &&
                          carbohydrates > 0 && (
                            <div
                              className="row mx-2 text-center mt-2"
                              style={{ fontSize: "20px" }}
                            >
                              <div className="col-12">
                                <p className="position-detail-collapse-content-text text-center mb-1">
                                  <span className="position-detail-span-bold">
                                    Калорий
                                  </span>
                                </p>
                                <p className="position-detail-collapse-content-text text-center mb-3">
                                  <span className="position-detail-calories">
                                    {calories}
                                  </span>
                                </p>
                              </div>

                              <div className="col-4 px-0">
                                <span className="position-detail-span-bold-dark">
                                  {proteins}
                                </span>
                                <br />
                                белков
                              </div>
                              <div className="col-3 px-0">
                                <span className="position-detail-span-bold-dark">
                                  {fats}
                                </span>
                                <br />
                                жиров
                              </div>
                              <div className="col-5 px-0">
                                <span className="position-detail-span-bold-dark">
                                  {carbohydrates}
                                </span>
                                <br />
                                углеводов
                              </div>
                            </div>
                          )}
                      </div>
                    </div>
                  )}
                <div className="d-flex align-items-center justify-content-center py-4">
                  <button
                    type="button"
                    className="position-detail-btn-cart px-4 py-2"
                    style={{ transition: "all .3s" }}
                    onClick={() => {
                      if (!position) return;
                      if (extend.length > 0) {
                        setShowAddition(true);
                      } else {
                        addToCart(position);
                      }
                    }}
                  >
                    Добавить
                  </button>
                </div>
              </div>
            </div>
          </div>
          {showAddition && (
            <AdditionPopup
              fromCart={fromCart}
              originalPosition={position}
              extend={extend}
              addToCart={addToCart}
              setShowAddition={setShowAddition}
            />
          )}
          <div className="position-absolute menu-position-detail-bg">
            <div className="bg-detail-yellow"></div>
            <div className="bg-detail-white position-absolute"></div>
          </div>
        </div>
      );
    })
  )
);
