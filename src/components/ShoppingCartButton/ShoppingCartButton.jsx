import React from "react";
import { inject, observer } from "mobx-react";
import { toJS } from "mobx";
import { Link } from "react-router-dom";
import "./ShoppingCartButton.css";

export const ShoppingCartButton = inject("rootStore")(
  observer((props) => {
    const savedNumber = toJS(props.rootStore.cartStore.saved).length;
    const location = window.location.href;
    return (
      <Link to="/shopping-cart" className="cart-btn-link">
        <div className="shopping-cart-button d-flex align-items-center justify-content-center position-relative">
          <svg
            className="hnf-svg-icon"
            focusable="false"
            xmlns="http://www.w3.org/2000/svg"
            width="30px"
            viewBox="0 0 24 24"
          >
            <path d="M16.677 10l-1.245-3.114L12.646 5h-1.292L8.568 6.886 7.323 10H2l2.544 7.633A2 2 0 0 0 6.441 19h11.117a2 2 0 0 0 1.898-1.368L22 10zm-6-3h2.646l1.2 3H9.477zm6.881 10H6.441l-1.666-5h14.45z"></path>
          </svg>
          {savedNumber > 0 && (
            <div
              key={savedNumber}
              className={`position-absolute rounded-circle d-flex align-items-center justify-content-center animate__animated ${
                location.includes("/menu-position") ? "animate__zoomInUp" : ""
              }`}
              style={{
                bottom: "-10px",
                right: "-6px",
                width: "25px",
                height: "25px",
                backgroundColor: "#0051BA",
                boxShadow: "0 4px 4px rgba(0,0,0,.3)",
                color: "white",
                fontSize: "12px",
                fontWeight: "bold",
              }}
            >
              {savedNumber}
            </div>
          )}
        </div>
      </Link>
    );
  })
);
