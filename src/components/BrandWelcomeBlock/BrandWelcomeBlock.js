import React from "react";

export function BrandWelcomeBlock() {
  return (
    <div
      style={{
        backgroundColor: "white",
        height: "50px",
        overflowX: "hiddem",
        boxShadow: "0 4px 8px rgba(0,0,0,.15)",
      }}
      className="d-flex align-items-center justify-content-start"
    >
      <img
        className="py-2 px-1"
        style={{ height: "100%" }}
        src="https://www.ikea.com/ru/ru/static/ikea-logo.f88b07ceb5a8c356b7a0fdcc9a563d63.svg"
        alt="IKEA logotype"
      />
      <p
        style={{ fontWeight: "bold", fontSize: "18px" }}
        className="mb-0 w-100 text-center text-md-left mr-4"
      >
        Приятного аппетита!
      </p>
    </div>
  );
}
