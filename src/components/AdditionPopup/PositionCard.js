import React from "react";

export const PositionCard = (props) => {
  const { title, price, portionSize, photo } = props.pos;
  return (
    <div
      className="addition-position-card mx-4 my-2 bg-white my-4"
      style={{ width: "300px", height: "275px" }}
    >
      <div style={{ height: "65%", width: "100%" }}>
        <img
          className="w-100 h-100"
          style={{
            objectFit: "cover",
            borderRadius: "18px 18px 0 0",
          }}
          src={photo}
          alt="123"
        />
      </div>
      <div
        className="addition-position-card-description d-flex flex-column align-items-center justify-content-between p-2"
        style={{ height: "35%" }}
      >
        <h2
          className="px-3 pt-2 mb-0 w-100 h-100 d-flex align-items-center"
          style={{ fontWeight: "bold" }}
        >
          {title}
        </h2>
        {portionSize && price && (
          <div className="d-flex justify-content-between px-3 w-100 pb-2">
            <span className="size">{portionSize}г</span>
            <span className="price">{price} ₽</span>
          </div>
        )}
      </div>
    </div>
  );
};
