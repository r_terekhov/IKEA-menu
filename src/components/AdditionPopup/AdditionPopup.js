import React, { useState } from "react";
import "./AdditionPopup.css";
import { PositionCard } from "./PositionCard";
import Swiper from "react-id-swiper";
import { useHistory } from "react-router-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { inject, observer } from "mobx-react";

const params = {
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
};

export const AdditionPopup = inject("rootStore")(
  observer((props) => {
    const { menuStore } = props.rootStore;

    const {
      extend,
      addToCart,
      setShowAddition,
      originalPosition,
      fromCart,
    } = props;

    const [swiper, setSwiper] = useState(null);
    const [selectedPositions, setSelectedPositions] = useState([]);
    const [currentStep, setCurrentStep] = useState(0);
    const history = useHistory();

    const currentExtend = extend[currentStep];

    const positions = menuStore.getPositionsByIds(currentExtend.positions);

    const currentChoiceEls = positions.map((position) => {
      return (
        <div key={position.id}>
          <PositionCard pos={position} />
        </div>
      );
    });

    const currentCart = (
      <div className="addition-card">
        <div className="addition-header-container d-flex flex-column justify-content-center align-items-center py-4 bg-white">
          <h1 style={{ fontWeight: "bold" }}>Выберите блюдо</h1>
          <h1 className="mb-0">{currentExtend.category}</h1>
        </div>
        <div className="d-flex flex-column align-items-center justify-content-center">
          <div className="w-100 mt-2">
            <TransitionGroup>
              <CSSTransition
                key={currentStep}
                timeout={{ enter: 200, exit: 0 }}
                classNames="fadeNoExit"
              >
                <div>
                  <Swiper {...params} getSwiper={setSwiper}>
                    {currentChoiceEls}
                  </Swiper>
                </div>
              </CSSTransition>
            </TransitionGroup>
          </div>
          <button
            className="addition-select-btn w-75 p-2"
            onClick={() => {
              setSelectedPositions((prev) => [
                ...prev,
                {
                  position: positions[Number(swiper.activeIndex)],
                },
              ]);
              if (currentStep + 1 >= extend.length) {
                addToCart(
                  Object.assign({}, originalPosition, {
                    extend: [
                      ...selectedPositions,
                      {
                        position: positions[Number(swiper.activeIndex)],
                      },
                    ],
                  })
                );
                if (fromCart) {
                  history.push("/shopping-cart");
                } else {
                  setShowAddition(false);
                }
              } else {
                setCurrentStep((prev) => prev + 1);
              }
            }}
          >
            Выбрать
          </button>
          <button
            className="addition-back-btn w-75 p-2 my-2"
            onClick={() => {
              if (currentStep > 0) {
                setCurrentStep((prev) => prev - 1);
                setSelectedPositions((prev) => prev.slice(0, -1));
              } else {
                setShowAddition(false);
              }
            }}
          >
            Назад
          </button>
        </div>
      </div>
    );
    return (
      <div
        className="addition-popup-container d-flex align-items-center justify-content-center"
        id="addition-popup-bg"
        onClick={(e) => {
          if (e.target.id === "addition-popup-bg") setShowAddition(false);
        }}
      >
        {currentCart}
      </div>
    );
  })
);
