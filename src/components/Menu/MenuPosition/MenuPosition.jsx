import React from "react";
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";
import "./MenuPosition.css";

export const MenuPosition = inject("rootStore")(
  observer((props) => {
    const { id, price, title, photo } = props.position;

    return (
      <Link
        to={`/menu-position/${id}`}
        className="menu-position-link-container"
      >
        <div className="position-relative" key={id}>
          <div className="square">
            <div
              className="w-100 h-100"
              style={{
                background: `url(${photo}) no-repeat`,
                backgroundSize: "cover",
                backgroundPosition: "center",
                position: "absolute",
              }}
            />
          </div>
          <p className="price-badge px-2 font-weight-bold">{price} ₽</p>
          <p className="h-100 text-center position-title px-0 p-md-2 mb-3 py-2 d-flex align-items-center justify-content-center">
            {title}
          </p>
        </div>
      </Link>
    );
  })
);
