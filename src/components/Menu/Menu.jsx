import React, { useEffect } from "react";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { toJS } from "mobx";
import { inject, observer } from "mobx-react";
import { withRouter, useParams, useHistory } from "react-router-dom";
import "./Menu.css";
import { MenuPosition } from "./MenuPosition/MenuPosition";
import { CategoryPosition } from "./CategoryPosition/CategoryPosition";

export const Menu = withRouter(
  inject("rootStore")(
    observer((props) => {
      const { category = "" } = useParams();
      const history = useHistory();
      const { menuStore } = props.rootStore;

      const currentCategory = category;
      const menuNavs = toJS(menuStore.menuNavs);

      const menuNavElements = menuNavs.map((category) => (
        <CSSTransition
          key={category.name}
          timeout={{ enter: 300, exit: 300 }}
          classNames="fade"
        >
          <div className="col-6 col-md-4 col-lg-3 px-md-4 mb-2">
            <CategoryPosition category={category} in />
          </div>
        </CSSTransition>
      ));
      const positions = menuStore.getPositionsByCategory(currentCategory);
      const positionEls = positions.map((pos) => (
        <CSSTransition
          key={pos.title}
          timeout={{ enter: 300, exit: 300 }}
          classNames="fade"
        >
          <div className="col-6 col-md-4 col-lg-3 px-md-4 mb-2">
            <MenuPosition position={pos} in />
          </div>
        </CSSTransition>
      ));

      useEffect(() => window.scrollTo(0, 0), []);

      return (
        <div
          className="container-menu"
          style={{ display: "flex", flexDirection: "column" }}
        >
          <div
            className={
              currentCategory === "" ? "container pt-4" : "container pt-4"
            }
            style={{ flex: "1 0 auto" }}
          >
            {currentCategory !== "" && (
              <h1
                className="reset-category px-3"
                onClick={() => history.push("/")}
              >
                <i
                  class="fas fa-chevron-left mr-1"
                  style={{ transform: "scale(0.75)" }}
                ></i>
                Назад
              </h1>
            )}
            {currentCategory === "" ? (
              <div className="div d-flex flex-column align-items-center justify-content-center mb-4">
                <h1
                  className="welcome-title text-center mb-0"
                  style={{ fontSize: "20px", whiteSpace: "pre-line" }}
                >
                  <span style={{ fontWeight: "bold" }}>Добро пожаловать</span>
                  <br />в онлайн-меню ресторана IKEA! Выберите подходящую
                  категорию.
                </h1>
              </div>
            ) : (
              <h1
                className="welcome-title p-3"
                style={{
                  fontSize: window.innerHeight > 960 ? "24px" : "20px",
                  fontWeight: "bold",
                }}
              >
                {currentCategory}
              </h1>
            )}
            <TransitionGroup className="row w-100 mx-0">
              {currentCategory === "" ? menuNavElements : positionEls}
              {/* <SpecialOffer /> */}
            </TransitionGroup>
          </div>
          <div
            className="text-center p-3 border-top"
            style={{
              fontSize: "12px",
              flexShrink: 0,
              color: "white",
              backgroundColor: "#0051BA",
            }}
          >
            Цены в разных магазинах ИКЕА могут отличаться. Не является публичной
            офертой. Подробности об ассортименте, ценах и условиях продажи
            уточняйте в магазинах.
          </div>
        </div>
      );
    })
  )
);
