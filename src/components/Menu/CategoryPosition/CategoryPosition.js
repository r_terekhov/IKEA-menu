import React from "react";
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";
import "../MenuPosition/MenuPosition.css";

export const CategoryPosition = inject("rootStore")(
  observer((props) => {
    const { menuStore } = props.rootStore;

    const { id, name, photo } = props.category;
    return (
      <Link to={`/category/${name}`} className="menu-position-link-container">
        <div
          className="position-relative"
          key={id}
          style={{ color: "rgba(0, 0, 0, 0.7)" }}
          onClick={(e) => menuStore.setCurrentCategory(name)}
        >
          <div className="square">
            <div
              className="w-100 h-100"
              style={{
                background: `url(${photo}) no-repeat`,
                backgroundSize: "cover",
                backgroundPosition: "center",
                position: "absolute",
              }}
            />
          </div>
          <p className="h-100 text-center position-title px-0 p-md-2 mb-3 py-2 d-flex align-items-center justify-content-center">
            {name}
          </p>
        </div>
      </Link>
    );
  })
);
