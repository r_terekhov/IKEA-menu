import React from "react";

export function ShoppingCartSubitem(props) {
  const { title, photo } = props.pos;
  return (
    <div
      className="row my-2 mx-0 p-2"
      style={{
        backgroundColor: "rgba(255,218,27,1)",
        boxShadow: "0 4px 16px rgba(0,0,0,.15)",
        borderRadius: "18px",
      }}
    >
      <div className="col-4 col-md-2 d-flex justify-content-center px-0">
        <img
          src={photo}
          alt={title}
          style={{
            width: "85px",
            height: "85px",
            objectFit: "cover",
            borderRadius: "14px",
            boxShadow: "0 4px 16px rgba(0,0,0,.15)",
          }}
        />
      </div>
      <div className="col-8 col-md-10 d-flex align-items-center">
        <h4 className="cart-subitem-title w-100 d-inline-block text-truncate">
          {title}
        </h4>
      </div>
    </div>
  );
}
