import React from "react";
import { inject, observer } from "mobx-react";
import { Link, useHistory } from "react-router-dom";
import { ShoppingCartSubitem } from "./ShoppingCartSubitem";
import "./ShoppingCartItem.css";

export const ShoppingCartItem = inject("rootStore")(
  observer((props) => {
    const {
      position,
      position: { id, photo, title, price, extend },
      quantity,
    } = props.savedPos;
    const { last } = props;

    const { addToCart, removeFromCart } = props.rootStore.cartStore;

    const extendEls = extend.map(({ position }) => (
      <ShoppingCartSubitem pos={position} />
    ));
    const history = useHistory();
    return (
      <div
        className="mb-3 pb-3"
        style={{ borderBottom: !last ? "1px solid black" : "none" }}
      >
        <div className="row mx-0">
          <div className="col-4 col-md-2 d-flex justify-content-center align-items-center">
            <Link to={`/menu-position/${id}`}>
              <img
                alt={title}
                src={photo}
                style={{
                  width: "95px",
                  height: "95px",
                  objectFit: "cover",
                  borderRadius: "14px",
                  boxShadow: "0 4px 16px rgba(0,0,0,.15)",
                }}
              />
            </Link>
          </div>
          <div className="col-8 col-md-10 d-flex flex-column justify-content-around">
            <div className="">
              <h4 className="cart-item-title w-100 d-inline-block">{title}</h4>
            </div>
            <div className="row">
              <div className="col-5 col-md-8 col-lg-10">
                <p className="cart-item-price my-2">{`${price} ₽`}</p>
              </div>
              <div className="col-7 col-md-4 col-lg-2">
                <div className="row cart-item-btn-wrapper mx-0 my-2">
                  <div
                    className="col-4 d-flex justify-content-center align-items-center cart-item-btn-icon"
                    onClick={() => removeFromCart(position)}
                  >
                    <i class="fas fa-minus"></i>
                  </div>
                  <div className="col-4 d-flex justify-content-center align-items-center cart-item-btn-quantity">
                    {quantity}
                  </div>
                  <div
                    className="col-4 d-flex justify-content-center align-items-center cart-item-btn-icon"
                    onClick={() => {
                      if (extendEls.length === 0) {
                        addToCart(position);
                      } else {
                        history.push(
                          `/menu-position/${position.id}?fromCart=true`
                        );
                      }
                    }}
                  >
                    <i class="fas fa-plus"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {extendEls.length > 0 && (
          <div className="row mt-3 mx-0">
            <div className="col-2 px-0"></div>
            <div className="col-10 px-0">
              <h5 className="cart-subitem-includes px-4 py-2">
                Включает в себя
              </h5>
              {extendEls}
            </div>
          </div>
        )}
      </div>
    );
  })
);
