import React, { useEffect } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import "./ShoppingCart.css";
import { ShoppingCartItem } from "./ShoppingCartItem/ShoppingCartItem";

export const ShoppingCart = withRouter(
  inject("rootStore")(
    observer((props) => {
      const { cartStore } = props.rootStore;
      const saved = cartStore.getSavedGroupedByQuantity();
      const totalPrice = cartStore.totalPrice.get();
      const goHome = () => {
        props.history.push("/");
      };

      const savedEls = saved.map((pos, index) => (
        <ShoppingCartItem
          savedPos={pos}
          last={index + 1 === saved.length}
          key={pos.position.id + index}
        />
      ));
      const totalPriceEl =
        totalPrice === 0 ? (
          ""
        ) : (
          <div className="d-flex justify-content-between mt-3 cart-total-price-container">
            <p>Итого:</p>
            <p className="cart-total-price">{`${totalPrice} ₽`}</p>
          </div>
        );

      useEffect(() => window.scrollTo(0, 0), []);

      return (
        <div
          className="position-relative cart-wrapper"
          style={{ overflowX: "hidden" }}
        >
          <div className="position-absolute cart-bg">
            <div className="cart-button-back pl-4" onClick={goHome}>
              <i className="fas fa-chevron-left"></i>
            </div>
            <div className="position-absolute cart-main-container">
              <div className="container">
                <h2 className="cart-title pt-3">Ваша корзина</h2>
                <h3 className="cart-position-number mb-3">
                  {savedEls.length > 0
                    ? `${saved.length} блюда`
                    : "Здесь пока ничего нет :("}
                </h3>
                {savedEls.length > 0 && (
                  <div className="cart-position-container d-flex flex-column py-3">
                    {savedEls}
                  </div>
                )}
                {totalPriceEl}
                {savedEls.length > 0 && (
                  <h3 className="text-center enjoy-your-meal-title py-3">
                    <span style={{ fontSize: "30px" }}>Smaklig måltid!</span>{" "}
                    <br />
                    <span style={{ fontSize: "22px" }}>[сма́клиг мо́лтид]</span>
                    <br />
                    Так мы говорим «Приятного аппетита» по-шведски
                  </h3>
                )}
              </div>
            </div>
          </div>
        </div>
      );
    })
  )
);
